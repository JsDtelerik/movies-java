package movie_product_data;

import movie_product_data.core.ControllerImpl;
import movie_product_data.core.EngineImpl;
import movie_product_data.core.interfaces.Controller;
import movie_product_data.core.interfaces.Engine;


import java.io.*;


public class Main {

    public static void main(String[] args) throws IOException {

        Controller controller = new ControllerImpl();
        Engine engine = new EngineImpl(controller);
        engine.run();

    }
}