package movie_product_data.repositories;

import movie_product_data.movie.Movie;

import java.util.ArrayList;
import java.util.Collection;


public class MovieRepository implements Repository<Movie>{

    private Collection<Movie> movieData;

    public MovieRepository() {
        this.movieData = new ArrayList<>();
    }


    @Override
    public Collection<Movie> getData() {
        return movieData;
    }

    @Override
    public void add(Movie data) {
        movieData.add(data);
    }

    @Override
    public int count() {
        return movieData.size();
    }



    @Override
    public Movie findById(int id) {
        Movie movieOfInterest = null;
        for (Movie movie : movieData) {
            if(movie.getId() == id){
                movieOfInterest = movie;

            }
        }
        return movieOfInterest;
    }



}
