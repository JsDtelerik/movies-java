package movie_product_data.repositories;

import movie_product_data.user.User;

import java.util.ArrayList;
import java.util.Collection;

public class UserRepository implements Repository<User>{

    private Collection<User> userData;

    public UserRepository() {
        this.userData = new ArrayList<>();
    }

    @Override
    public Collection<User> getData() {
        return userData;
    }

    @Override
    public void add(User data) {
        userData.add(data);
    }

    @Override
    public int count() {
        return userData.size();
    }



    @Override
    public User findById(int id) {
        User currentBrowsingUser = null;
        for (User user : userData) {
            if(user.getId() == id){
                currentBrowsingUser = user;

            }
        }

       return currentBrowsingUser;
    }


    public void setUserCurrentSessionMovie(int userId, int movieId){

        User foundUser = findById(userId);
        if(foundUser !=null){
            foundUser.setCurrentSession(movieId);
        }else{
            throw new IllegalArgumentException(String.format("User with ID - %d does not exist in our database"));
        }
    }
}
