package movie_product_data.repositories;

import java.util.Collection;

public interface Repository<T> {

        Collection<T> getData();

        void add(T data);

        int count();


        T findById (int id);


}
