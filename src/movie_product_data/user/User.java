package movie_product_data.user;

import movie_product_data.movie.Movie;

import java.util.*;
import java.util.stream.Collectors;

public class User {


    //id, name, viewed (products separated by ;), purchased (products separated by ;)


    private int id;
    private String name;
    private List<Integer> previewedMoviesId;
    private List<Integer> purchasedMoviesId;
    private int currentSession;
    private List<Movie> recommendedMovies;
    private List<Integer> boughtAndPreviewed;
    private List<String> frequentlyPattern;


    public User (int inputId, String inputName, String inputPreviewed, String inputPurchased ){
        this.setId(inputId);
        this.setName(inputName);
        this.previewedMoviesId = Arrays.stream(inputPreviewed.split(";")).map(Integer::parseInt)
               .collect(Collectors.toList());
        this.purchasedMoviesId = Arrays.stream(inputPurchased.split(";")).map(Integer::parseInt)
               .collect(Collectors.toList());
        this.currentSession = 0;
        this.recommendedMovies = new ArrayList<>();
        this.setBoughtAndPreviewed();
        this.frequentlyPattern = new ArrayList<>();
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Integer> getPreviewedMoviesId() {
        return previewedMoviesId;
    }

    public List<Integer> getPurchasedMoviesId() {
        return purchasedMoviesId;
    }

    private void setId (int inputId){
        if(inputId <= 0){
            throw new IllegalArgumentException("Users ID must be a positive number greater than zero. " +
                    "Please, check Your Users.txt file and try again.");
        }
        this.id = inputId;
    }

    private void setName (String inputName){
        if(inputName == null || inputName.trim().isEmpty() || inputName.length() <= 1){
            throw new IllegalArgumentException("Users name must be at least two letters long. Please, check Your Users.txt file and try again.");
        }
        this.name = inputName;
    }

    public int getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(int currentSession) {
        this.currentSession = currentSession;
    }

    public List<Movie> getRecommendedMovies() {
        return recommendedMovies;
    }

    public void setRecommendedMovies(Movie recommendedMovie) {
        this.recommendedMovies.add(recommendedMovie);
    }

    public List<Integer> getBoughtAndPreviewed() {
        return boughtAndPreviewed;
    }

    private void setBoughtAndPreviewed() {
        this.boughtAndPreviewed = new ArrayList<>();
        this.previewedMoviesId.stream().forEach(movieId -> this.boughtAndPreviewed.add(movieId));
        this.purchasedMoviesId.stream().forEach(movieId -> {
            if(!this.boughtAndPreviewed.contains(movieId)){
                this.boughtAndPreviewed.add(movieId);
            }
        } );
    }

    public List<String> getFrequentlyPattern() {
        return this.frequentlyPattern;
    }

    public void setFrequentlyPattern(List<String> frequentlyPattern) {

        this.frequentlyPattern = frequentlyPattern;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(System.lineSeparator())
                .append("Hi, ").append(getName()).append(" we can recommend to you the following list of movies: ")
                .append(System.lineSeparator());

        for (Movie recommendedMovie : recommendedMovies) {
            builder.append(recommendedMovie.toString());

        }

        return builder.toString();

    }



}
