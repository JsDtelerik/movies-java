package movie_product_data.movie;

import java.util.List;

public class Movie {

    //id, name, year, keyword 1, keyword 2, keyword 3, keyword 4, keyword 5, rating, price

    private int id;
    private String name;
    private int year;
    private List<String> genre;
    private double averageUsersReview;
    private double price;
    private int views;

    public Movie (int inputId, String inputName, int inputYear, List<String> inputGenre, double inputAvgReview, double inputPrice){
        this.setId(inputId);
        this.setName(inputName);
        this.setYear(inputYear);
        this.genre = inputGenre;
        this.setAverageUsersReview(inputAvgReview);
        this.setPrice(inputPrice);
        this.views = 0;
    }

    public int getId() {

        return this.id;
    }

    public String getName() {

        return this.name;
    }

    public int getYear() {

        return this.year;
    }

    public List<String> getGenre() {
        return this.genre;
    }

    public double getAverageUsersReview() {

        return this.averageUsersReview;
    }

    public double getPrice() {

        return this.price;
    }


    private void setId(int inputID){
        if(inputID<=0){
            throw new IllegalArgumentException("Movie ID must be a positive number greater than zero. Please, check Your Products.txt file.");
        }
        this.id = inputID;
    }
    private void setName(String inputName){
        if(inputName == null || inputName.trim().isEmpty()){
            throw new IllegalArgumentException("Movie name must contain at least one letter. Please, check Your Products.txt file.");
        }
        this.name =inputName;
    }
    private void setYear (int inputYear){
        if(inputYear<1878){
            throw new IllegalArgumentException("The first movie was presented in 1878. Please, check Your Products.txt file.");
        }
        this.year = inputYear;
    }

    private void setAverageUsersReview (double inputAvg){
        if(inputAvg < 0.0 || inputAvg > 5.0){
            throw new IllegalArgumentException("Movie must be evaluated with a score between 0.0 and 5.0. Please, check Your Products.txt file.");
        }
        this.averageUsersReview = inputAvg;
    }

    private void setPrice(double inputPrice){
        if(inputPrice < 1){
            throw new IllegalArgumentException("Movie price must be a positive number greater or equal to 1. Please, check Your Products.txt file.");
        }
        this.price = inputPrice;
    }

    public int getViews() {
        return views;
    }

    public void setViews() {
        this.views += 1;
    }

    @Override
    public String toString() {
        return "Movie title: " + getName() + ", release year: " + getYear() + ", genre: " + String.join(" ", getGenre()) + "\n";
    }


}
