package movie_product_data.core.interfaces;




import java.io.IOException;


public interface Controller {

    void loadInformation (String userDataPath, String movieDataPath, String currentSession) throws IOException;


    String recommendMoviesToOurUsers();


}
