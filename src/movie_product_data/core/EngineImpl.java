package movie_product_data.core;

import movie_product_data.core.interfaces.Controller;
import movie_product_data.core.interfaces.Engine;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class EngineImpl implements Engine {


    private Controller controller;
    private BufferedReader readFileDirectory;

    public EngineImpl(Controller controller) {
        this.controller = controller;
        this.readFileDirectory = new BufferedReader(new InputStreamReader(System.in));
    }


    @Override
    public void run() {
        String result = null;
        try {
            result = processInput();


        } catch (NullPointerException | IllegalArgumentException | IOException e) {
            result = e.getMessage();
        }

        System.out.println(result);
    }


    private String processInput() throws IOException {
        System.out.printf("Please, enter the path to Your users text file.%n");
        System.out.printf("Example: C:\\.....\\Noroff project\\src\\Movie product data\\Users.txt.%n");
        String usersInput = this.readFileDirectory.readLine();

        System.out.printf("Please, enter the path to Your movies text file.%n");
        System.out.printf("Example: C:\\.....\\Noroff project\\src\\Movie product data\\Products.txt.%n");
        String movieInput = this.readFileDirectory.readLine();

        System.out.printf("Please, enter the path to Your CurrentSession.text file.%n");
        System.out.printf("Example: C:\\....\\Noroff project\\src\\Movie product data\\CurrentUserSession.txt.%n");
        String currentSessionInput = this.readFileDirectory.readLine();

         controller.loadInformation(usersInput, movieInput, currentSessionInput);

        String result = controller.recommendMoviesToOurUsers();


        return result;
    }
}


