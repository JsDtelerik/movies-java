package movie_product_data.core;

import movie_product_data.core.interfaces.Controller;
import movie_product_data.movie.Movie;
import movie_product_data.repositories.MovieRepository;
import movie_product_data.repositories.UserRepository;
import movie_product_data.user.User;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ControllerImpl implements Controller {
   private static List<Integer> currentSessionUsers = new ArrayList<>(); 
   private static MovieRepository moviesCollection;
   private static UserRepository usersCollection;



    public ControllerImpl(){
        this.usersCollection = new UserRepository();
        this.moviesCollection = new MovieRepository();
    }




    @Override
    public void loadInformation(String userDataPath, String movieDataPath, String currentSession) throws IOException {
          fillUsersData(userDataPath);
          fillMovieData(movieDataPath);
          fillCurrentUserSessions(currentSession);

    }

    private void fillCurrentUserSessions(String currentSession) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(currentSession));
        
        String input;

        while((input = reader.readLine()) != null){
            String[] splitInput = input.split(",");
            int currentSessionUserId = Integer.parseInt(splitInput[0].trim());
            try{
                usersCollection.setUserCurrentSessionMovie(currentSessionUserId, Integer.parseInt(splitInput[1].trim()));
                currentSessionUsers.add(currentSessionUserId);
            }catch (NullPointerException | IllegalArgumentException e){
                System.out.println(e.getMessage());
            }

        }

      }

    @Override
    public String recommendMoviesToOurUsers() {
        List<Movie> sortedByRating = moviesCollection.getData().stream()
                .sorted(Comparator.comparingDouble(Movie::getAverageUsersReview).reversed())
                .sorted(Comparator.comparingInt(Movie::getViews).reversed())
                .collect(Collectors.toList());

        
        matchMoviesPerEachCurrentSessionUser(sortedByRating);

        StringBuilder builder = new StringBuilder();
        builder.append(System.lineSeparator())
                .append("If you have free time to watch movies, we would like to recommend you the following titles:")
                .append(System.lineSeparator())
                .append(sortedByRating.get(0).toString())
                .append(sortedByRating.get(1).toString())
                .append(sortedByRating.get(2).toString())
                .append(sortedByRating.get(3).toString())
                .append(sortedByRating.get(4).toString())
                .append(System.lineSeparator());


        for (Integer currentSessionUser : currentSessionUsers) {
            builder.append(usersCollection.findById(currentSessionUser).toString());
        }


        return builder.toString();
    }

    private void matchMoviesPerEachCurrentSessionUser(List<Movie> sortedByRating) {

        for (Integer currentSessionUserId : currentSessionUsers) {

            List<String> watchedAndBoughtGenrePattern = setCurrentUserFrequentlyPattern(usersCollection
                    .findById(currentSessionUserId));
            int currentSessionMovieId = usersCollection.findById(currentSessionUserId).getCurrentSession();

            matchByPattern(currentSessionUserId, currentSessionMovieId, sortedByRating, "frequentlyPattern");
            matchByPattern(currentSessionUserId, currentSessionMovieId, sortedByRating, "currentSession");
        }
    }
        private void matchByPattern (int currentSessionUserId, int currentSessionMovieId, List<Movie> sortedByRating, String pattern) {
            List<String> currentSessionReviewedMovieGenre = new ArrayList<>(moviesCollection.findById(currentSessionMovieId)
                    .getGenre());

            int match = 0;
            label:
            for (Movie movie : sortedByRating) {

                second:
                if (movie.getId() != currentSessionMovieId) {
                    for (Integer boughtOrPreviewedMovieId : usersCollection.findById(currentSessionUserId).getBoughtAndPreviewed()) {
                        if (movie.getId() == boughtOrPreviewedMovieId) {

                            break second;
                        }
                    }
                if(pattern.equals("currentSession")){
                    for (String genre : currentSessionReviewedMovieGenre) {
                        if (movie.getGenre().contains(genre)
                                && !usersCollection.findById(currentSessionUserId).getRecommendedMovies().contains(movie)) {

                            usersCollection.findById(currentSessionUserId).setRecommendedMovies(movie);
                            match++;
                            if (match == 3) {
                                break label;
                            }
                            break;

                        }
                    }
                }else if (pattern.equals("frequentlyPattern")){
                    List<String> genrePattern = usersCollection.findById(currentSessionUserId).getFrequentlyPattern();
                        if (movie.getGenre().contains(genrePattern.get(0)) && movie.getGenre().contains(genrePattern.get(1))
                             && movie.getGenre().contains(genrePattern.get(2)) && !usersCollection.findById(currentSessionUserId).getRecommendedMovies().contains(movie)) {
                            usersCollection.findById(currentSessionUserId).setRecommendedMovies(movie);

                        }
                    }
                }
            }


        }


    private List<String> setCurrentUserFrequentlyPattern(User currentUser) {
        LinkedHashMap<String, Integer> genreMapCounter = new LinkedHashMap<>();
        for (Integer boughtMovieId : currentUser.getPurchasedMoviesId()) {
            if(currentUser.getBoughtAndPreviewed().contains(boughtMovieId)){
                for (String movieGenre : moviesCollection.findById(boughtMovieId).getGenre()) {
                    if(genreMapCounter.containsKey(movieGenre)){
                        genreMapCounter.put(movieGenre, genreMapCounter.get(movieGenre)+1);
                    }else{
                        genreMapCounter.put(movieGenre, 1);
                    }
                }
            }
        }
        List<String> topThreeGenre = new ArrayList<>();
        genreMapCounter.entrySet().stream()
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .forEach(e -> topThreeGenre.add(e.getKey()));

        currentUser.setFrequentlyPattern(topThreeGenre);

        return currentUser.getFrequentlyPattern();
    }


    public void fillMovieData(String movieDataPath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(movieDataPath));

        String input;

        while((input = reader.readLine()) != null){
            String[] splitInput = input.split(",");
            List<String> genre = separateGenreInfo(splitInput);
            try{
                Movie movie = new Movie(Integer.parseInt(splitInput[0]), splitInput[1], Integer.parseInt(splitInput[2]),
                        genre, Double.parseDouble(splitInput[8]), Integer.parseInt(splitInput[9]));

                moviesCollection.add(movie);
            }catch (NullPointerException | IllegalArgumentException e){
                System.out.println(e.getMessage());
            }

        }


        addViews();


    }



    private void addViews() {
        for (User user : usersCollection.getData()) {
            for (Integer purchasedId : user.getPurchasedMoviesId()) {
                if(moviesCollection.findById(purchasedId) != null ){
                    moviesCollection.findById(purchasedId).setViews();
                }
            }
        }
    }


    public void fillUsersData(String userDataPath) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(userDataPath));
        String input;

        while((input = reader.readLine()) != null){
            String[] splitInput = input.split(",");
            try {
                User user = new User(Integer.parseInt(splitInput[0]), splitInput[1].replaceAll(" ",""),
                        splitInput[2].replaceAll(" ", ""), splitInput[3].replaceAll(" ", ""));

                usersCollection.add(user);
            }catch  (NullPointerException | IllegalArgumentException e) {
                System.out.println(e.getMessage());

            }


        }


    }



    public static List<String> separateGenreInfo(String[] splitInput) {
        List<String> genre = new ArrayList<>();

        for (int i = 3; i < splitInput.length-2; i++) {
            String currentString = splitInput[i];
            if(!currentString.equals(" ")){

                genre.add(currentString.replaceAll(" ", ""));
            }
        }


        return genre;
    }



}


